var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var _ = require('lodash');

const originalPayload = require('./payload.json');
//fs.writeFileSync('./payload.json', JSON.stringify(originalPayload));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

let start = 0;
let times = 1000;
let number = 0;

function coordsToBuffer(coords) {
    let buffer = Buffer.from(new Array(coords.length * 8));
    let offset = 0;
    coords.forEach(point => {
        buffer.writeFloatBE(point.x, offset);
        offset += 4;
        buffer.writeFloatBE(point.y, offset);
        offset += 4;
    });
    return buffer;
}

io.on('connection', function (socket) {

    socket.on('finish', (payloadType) => {
        let finish = Date.now();
        let time = finish - start;
        console.log('- payload nº %s - %s - %s ms -', number, payloadType, time);
        number++;
    });

    socket.on('original', () => {
        start = Date.now();
        for (let i = 0; i < times; i++) {
            io.emit('originalPayload', originalPayload, i);
        }
    });

    socket.on('buffCord', () => {
        console.log('start buffered');
        let payloads = [];
        console.log('start cloneDeep');
        for (let i = 0; i < times; i++) {
            payloads[i] = _.cloneDeep(originalPayload);
        }
        console.log('finish cloneDeep');
        start = Date.now();
        console.log('start proccessing');
        for (let i = 0; i < times; i++) {
            let payload = payloads[i];
            payload.forEach((channel) => {
                channel.ascan_data.coords = coordsToBuffer(channel.ascan_data.coords).toString('base64');
            });
            payload = Buffer.from(JSON.stringify(payload));
            io.emit('buffCords', payload, i);
        }
        console.log('finish buffered');
    });

});

http.listen(3000, function () {
    console.log('listening on *:3000');
});